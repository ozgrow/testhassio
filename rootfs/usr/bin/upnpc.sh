#!/usr/bin/with-contenv bashio
#~/bin/upnpPortMapper.sh
#sudo apt-get install miniupnpc
#crontab -l | grep upnp || echo $(crontab -l ; echo '*/5 * * * * ~/bin/upnpPortMapper.sh  >/dev/null 2>&1') | crontab -
CONFIG_PATH=/data/options.json

bashio::log.info "Starting script upnpc"
export LC_ALL=C
bashio::log.debug "router..."
router=$(ip r | grep default | cut -d " " -f 3)
bashio::log.debug $router
bashio::log.debug "gateway..."
gateway=$(bashio::config "IP_or_DNS_local")
gateway=$router
bashio::log.info "fin gateway"
bashio::log.debug $gateway
bashio::log.debug "ip..."
ip=$(bashio::core info hostname)

bashio::log.debug $(bashio::network.ipv4_address)
bashio::log.debug "fin ip..."

external=$(bashio::config "port_externe")
port=$(bashio::config "port_local")
bashio::log.debug "upnpc -d ..."
upnpc -u  $gateway -d $external TCP
bashio::log.debug "upnpc -e..."
upnpc -u  $gateway -e "Web mapping for RaspberryPi" -a $ip $port $external TCP 
bashio::log.debug "fin upnpc -e..."
bashio::log.info "upnpc done"
